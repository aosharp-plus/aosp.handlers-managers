﻿namespace SyncManager.IPCMessages
{
    public enum IPCOpcode
    {
        Move = 200,
        Target = 201,
        Attack = 202,
        Use = 203,
        NpcChat = 204,
        StartStop = 205,
        UISettings = 206,
        Spread = 207,
        NPCStartTrade = 208,
        NPCTrade = 209,
        NPCFinishTrade = 210,
    }
}
