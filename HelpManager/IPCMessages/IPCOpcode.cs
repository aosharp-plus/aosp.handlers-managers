﻿namespace HelpManager.IPCMessages
{
    public enum IPCOpcode
    {
        YalmAction = 300,
        //YalmUse = 301,
        //YalmOff = 302,
        UISettings = 303,
        POHPathing = 304,
        POHBool = 305,
        Hoverboard = 306,
        //HoverboardOn = 306,
        //HoverboardUse = 307,
        //HoverboardOff = 308,
    }
}
